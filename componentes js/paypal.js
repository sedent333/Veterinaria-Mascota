const amountElement = document.getElementById("amount");

paypal.Buttons({
    createOrder: function(data,actions) {
        return actions.order.create({
            purchase_units: [{
                amount: {
                    value: amountElement.getAttribute('data-value'),
                }
            }]
        })
    },
    onApprove: function(data, actions) {                
        return actions.order.capture().then(function(details) {
            // Show a success message to the buyer
            alert('Transacción completada por ' + details.payer.name.given_name + '!');

            document.cookie="profile_viewer_uid=1";

            window.location = "http://localhost:3000/php/confirmar_compra.php";
    
           /* 
           esto envia una variable a compras_realizadas.php mediante parametros codificados en la url
           const msg = 'password';

            let normalUrl = new URL("http://localhost:3000/php/compras_realizadas.php");
            const myUrl = new URL(normalUrl);
            myUrl.searchParams.append('param', msg);
            const myOtherUrl = new URL(normalUrl);
            myOtherUrl.searchParams.append('url', myUrl.href);

            window.location = myOtherUrl.href;

            */
        });
    }
}).render('#paypal-button-container');