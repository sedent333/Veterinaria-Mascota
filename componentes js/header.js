const headerCall = document.querySelector(".header");

let numberMenu = 1;

const hidenMenu = $q(".navBarMenu"),
    NavLine1 = $q(".header__navBar__line:nth-of-type(1)"),
    NavLine2 = $q(".header__navBar__line:nth-of-type(2)");

//crea una x en el navbar si numberMenu es igual a 1 y lo desarma si no lo es
$q(".header__navBar").addEventListener("click", () => {

    NavLine1.style.animation = "navBarClickedLine1 0.5s Forwards";
    NavLine2.style.animation = "navBarClickedLine2 0.5s Forwards";
    if (numberMenu == 1) {
        numberMenu++;
        hidenMenu.style.animation = "navBarShow 1s forwards";
    } else {
        numberMenu--;
        hidenMenu.style.animation = "navBarHide 1s forwards";
        NavLine1.style.animation = "navBarClosingLine1 0.5s Forwards";
        NavLine2.style.animation = "navBarClosingLine2 0.5s Forwards";
    }
});

let closingNavBar = $qa('.navBarMenu a');
for (let i = 0; i < closingNavBar.length; i++) {
    closingNavBar[i].addEventListener("click", () => {

        NavLine1.style.animation = "navBarClickedLine1 0.5s Forwards";
        NavLine2.style.animation = "navBarClickedLine2 0.5s Forwards";
        if (numberMenu == 1) {
            numberMenu++;
            hidenMenu.style.animation = "navBarShow 1s forwards";
        } else {
            numberMenu--;
            hidenMenu.style.animation = "navBarHide 1s forwards";
            NavLine1.style.animation = "navBarClosingLine1 0.5s Forwards";
            NavLine2.style.animation = "navBarClosingLine2 0.5s Forwards";
        }
    });
}

let hideFunction = $class('hideMenuClose');
for (let i = 0; i < hideFunction.length; i++) {
    hideFunction[i].addEventListener("click", () => {
        if (numberMenu == 2) {
            numberMenu--;
            hidenMenu.style.animation = "navBarHide 1s forwards";
            NavLine1.style.animation = "navBarClosingLine1 0.5s Forwards";
            NavLine2.style.animation = "navBarClosingLine2 0.5s Forwards";
        }
    });
}