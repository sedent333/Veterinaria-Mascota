var apiFile = "../json/productos.json";
    const display = $q(".stock__tarjeta");

    const GetData = async () => {
        const res = await fetch(apiFile);
        const data = await res.json();
        return data
    }

    const DisplayData = async () => {
        const payload = await GetData();
    
        let USDollar = new Intl.NumberFormat('de-DE', {
            currency: 'USD',
            minimumFractionDigits: 2,
        });
    
        // Crear el formulario que contendrá todos los productos
        let dataDisplay = `
            <form id="formularioProductos" method="POST" action="agregarAlCarro.php">
        `;
    
        // Generar inputs para cada producto
        dataDisplay += payload.map((object) => {
            const { id, imagen, descripcion, precio, stock, caracteristicas } = object;
        
            return `
                <div class="stock__tarjeta__contenedor">
                    <img src="${imagen}" class="stock__tarjeta__contenedor__imagen" />
                    <div class="stock__tarjeta__contenedor__descripcion">
                        <div>${descripcion}</div>
                    </div>
                    <div class="stock__tarjeta__contenedor__precio">
                        <div>$${USDollar.format(precio)}</div>
                    </div>
                    <div class="stock__tarjeta__contenedor__stock">
                        ${esAdministrador == 1 ? `<div>Stock: ${stock}</div>` : `<div>Stock: No disponible</div>`}
                    </div>
                    <div class="stock__tarjeta__contenedor__caracteristicas">
                        <div class="stock__tarjeta__contenedor__caracteristicas">Características</div>
                        <ul>
                            ${caracteristicas[0] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[0]}</li>` : ""}
                            ${caracteristicas[1] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[1]}</li>` : ""}
                            ${caracteristicas[2] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[2]}</li>` : ""}
                            ${caracteristicas[3] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[3]}</li>` : ""}
                            ${caracteristicas[4] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[4]}</li>` : ""}
                        </ul>
                    </div>
                    <input id="productoId" name="productoId[]" type="hidden" value="${id}"/>
                    <input id="productoImagen" name="productoImagen[]" type="hidden" value="${imagen}"/>
                    <input id="productoDescripcion" name="productoDescripcion[]" type="hidden" value="${descripcion}"/>
                    <input id="productoPrecio" name="productoPrecio[]" type="hidden" value="${precio}"/>
                    <input id="productoStock" name="productoStock[]" type="hidden" value="${stock}"/>
                    ${caracteristicas[0] !== undefined ? `<input id="productoCaracteristicas" name="productoCaracteristicas_1[]" type="hidden" value="${caracteristicas[0]}"/>` : ''}
                    ${caracteristicas[1] !== undefined ? `<input id="productoCaracteristicas" name="productoCaracteristicas_2[]" type="hidden" value="${caracteristicas[1]}"/>` : ''}
                    ${caracteristicas[2] !== undefined ? `<input id="productoCaracteristicas" name="productoCaracteristicas_3[]" type="hidden" value="${caracteristicas[2]}"/>` : ''}
                    ${caracteristicas[3] !== undefined ? `<input id="productoCaracteristicas" name="productoCaracteristicas_4[]" type="hidden" value="${caracteristicas[3]}"/>` : ''}
                    ${caracteristicas[4] !== undefined ? `<input id="productoCaracteristicas" name="productoCaracteristicas_5[]" type="hidden" value="${caracteristicas[4]}"/>` : ''}
                    <input type="number" name="productoCantidad[]" placeholder="Cantidad" min="1" />
                </div>
            `;
        }).join("");
        
    
        // Cerrar el formulario y agregar un solo botón de submit
        dataDisplay += `
            <button type="submit" name="btnAgregarAlCarro">Agregar al carro</button>
            </form>
        `;
    
        // Insertar el formulario en el contenedor principal
        display.innerHTML = dataDisplay;
    
        // Agregar evento submit para validar antes de enviar
        document.getElementById("formularioProductos").addEventListener("submit", function(event) {
            // Prevenir el envío por defecto
            event.preventDefault();
    
            // Obtener todos los inputs de cantidad
            const cantidades = document.querySelectorAll('input[name="productoCantidad[]"]');
            let valid = false;
    
            // Verificar si alguna cantidad fue ingresada
            cantidades.forEach((input, index) => {
                if (input.value && parseInt(input.value) > 0) {
                    valid = true;
                } else {
                    // Si la cantidad no es válida, eliminar los inputs correspondientes al producto
                    input.closest('.stock__tarjeta__contenedor').querySelectorAll('input[type="hidden"]').forEach(hiddenInput => {
                        hiddenInput.remove();
                    });
                    input.remove(); // También eliminamos el input de cantidad vacío
                }
            });
    
            // Si hay al menos un producto con cantidad válida, se envía el formulario
            if (valid) {
                this.submit(); // Enviar el formulario si todo es correcto
            } else {
                alert("Debes agregar al menos un producto con una cantidad válida.");
            }
        });
    }
    
    DisplayData();
    
    
    


const filtrosRef = $q(".stock__filtro__texto");

const MostrarFiltros = () => {
    $q("#filtro0").addEventListener("click", function() {
        filtrosRef.innerHTML = "Sin filtro";
        $q(".stock__tarjeta").innerHTML = ""
        

    apiFile = "../json/productos.json";
    

    const DisplayData = async () => { 
        const payload = await GetData();
    
        let USDollar = new Intl.NumberFormat('de-DE', {
            currency: 'USD',
            minimumFractionDigits: 2,
        });
    
        // Crea el formulario que envolverá todos los productos
        let dataDisplay = `
            <form id="formularioProductos" method="POST" action="agregarAlCarro.php">
        `;
    
        // Generar inputs para cada producto
        dataDisplay += payload.map((object) => {
            const { id, imagen, descripcion, precio, stock, caracteristicas } = object;
    
            return `
                <div class="stock__tarjeta__contenedor">
                    <img src="${imagen}" class="stock__tarjeta__contenedor__imagen" />
    
                    <div class="stock__tarjeta__contenedor__descripcion">
                        <div>${descripcion}</div>
                    </div>
    
                    <div class="stock__tarjeta__contenedor__precio">
                        <div>$${USDollar.format(precio)}</div>
                    </div>
    
                    <div class="stock__tarjeta__contenedor__stock">
                        <div>Stock: ${stock}</div>
                    </div>
    
                    <div class="stock__tarjeta__contenedor__caracteristicas">
                        <div class="stock__tarjeta__contenedor__caracteristicas">Características</div>
                        <ul>
                            ${caracteristicas[0] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[0]}</li>` : ""}
                            ${caracteristicas[1] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[1]}</li>` : ""}
                            ${caracteristicas[2] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[2]}</li>` : ""}
                            ${caracteristicas[3] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[3]}</li>` : ""}
                            ${caracteristicas[4] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[4]}</li>` : ""}
                        </ul>
                    </div>
    
                    <input id="productoId" name="productoId[]" type="hidden" value="${id}"/>
                    <input id="productoImagen" name="productoImagen[]" type="hidden" value="${imagen}"/>
                    <input id="productoDescripcion" name="productoDescripcion[]" type="hidden" value="${descripcion}"/>
                    <input id="productoPrecio" name="productoPrecio[]" type="hidden" value="${precio}"/>
                    <input id="productoStock" name="productoStock[]" type="hidden" value="${stock}"/>
                    ${caracteristicas[0] !== undefined ? `<input id="productoCaracteristicas" name="productoCaracteristicas_1[]" type="hidden" value="${caracteristicas[0]}"/>` : ''}
                    ${caracteristicas[1] !== undefined ? `<input id="productoCaracteristicas" name="productoCaracteristicas_2[]" type="hidden" value="${caracteristicas[1]}"/>` : ''}
                    ${caracteristicas[2] !== undefined ? `<input id="productoCaracteristicas" name="productoCaracteristicas_3[]" type="hidden" value="${caracteristicas[2]}"/>` : ''}
                    ${caracteristicas[3] !== undefined ? `<input id="productoCaracteristicas" name="productoCaracteristicas_4[]" type="hidden" value="${caracteristicas[3]}"/>` : ''}
                    ${caracteristicas[4] !== undefined ? `<input id="productoCaracteristicas" name="productoCaracteristicas_5[]" type="hidden" value="${caracteristicas[4]}"/>` : ''}
    
                    <input type="number" name="productoCantidad[]" placeholder="Cantidad" min="1" />
                </div>
            `;
        }).join("");
    
        // Cerrar el formulario y agregar un solo botón de submit
        dataDisplay += `
            <button type="submit" name="btnAgregarAlCarro">Agregar al carro</button>
            </form>
        `;
    
        // Insertar el formulario en el contenedor principal
        display.innerHTML = dataDisplay;
    }
    
    DisplayData();
    

    DisplayData();
})

$q("#filtro1").addEventListener("click", function() {
    filtrosRef.innerHTML = "Filtrar por categoria: Alimentos";
    $q(".stock__tarjeta").innerHTML = ""

    apiFile = "../json/alimentos.json";

    const DisplayData = async () => {

        const payload = await GetData();

        let dataDisplay = payload.map((object) => {
            const { id, imagen, descripcion, precio, stock ,caracteristicas } = object;

            return `
                <div class="stock__tarjeta__contenedor">
                    <img src="${imagen}" class="stock__tarjeta__contenedor__imagen" />

                    <div class="stock__tarjeta__contenedor__descripcion">
                        <div>${descripcion}</div>
                    </div>

                    <div class="stock__tarjeta__contenedor__precio">
                        <div>$${precio}</div>
                    </div>

                    <div class="stock__tarjeta__contenedor__stock">
                        <div>Stock: ${stock}</div>
                    </div>

                    <div class="stock__tarjeta__contenedor__caracteristicas">
                        <div class="stock__tarjeta__contenedor__caracteristicas">Características</div>
                        <ul>
                            ${caracteristicas[0] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[0]}</li>` : ""}
                            ${caracteristicas[1] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[1]}</li>` : ""}
                            ${caracteristicas[2] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[2]}</li>` : ""}
                            ${caracteristicas[3] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[3]}</li>` : ""}
                            ${caracteristicas[4] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[4]}</li>` : ""}
                        </ul>
                    </div>
                    <form action="agregarAlCarro.php" method="POST">
                        <input id="productoId" name="productoId" type="hidden" value="${id}"/>
                        <input id="productoImagen" name="productoImagen" type="hidden" value="${imagen}"/>
                        <input id="productoDescripcion" name="productoDescripcion" type="hidden" value="${descripcion}"/>
                        <input id="productoPrecio" name="productoPrecio" type="hidden" value="${precio}"/>
                        <input id="productoStock" name="productoStock" type="hidden" value="${stock}"/>
                        <input id="productoCaracteristicas" name="productoCaracteristicas_1" type="hidden" value="${caracteristicas[0]}"/>
                        <input id="productoCaracteristicas" name="productoCaracteristicas_2" type="hidden" value="${caracteristicas[1]}"/>
                        <input id="productoCaracteristicas" name="productoCaracteristicas_3" type="hidden" value="${caracteristicas[2]}"/>
                        <input id="productoCaracteristicas" name="productoCaracteristicas_4" type="hidden" value="${caracteristicas[3]}"/>
                        <input id="productoCaracteristicas" name="productoCaracteristicas_5" type="hidden" value="${caracteristicas[4]}"/>

                        <input type="number" name="productoCantidad" placeholder="Agregar al carro" />
                        <input type="submit" name="btnAgregarAlCarro" placeholder="Agregar al carro" />
                    </form>
                </div>
            `
        }).join("");
        display.innerHTML = dataDisplay;
    }

    DisplayData();
})

$q("#filtro2").addEventListener("click", function() {
        filtrosRef.innerHTML = "Filtrar por categoria: Camas";
        $q(".stock__tarjeta").innerHTML = ""

    apiFile = "../json/camas.json";

    const DisplayData = async () => {

        const payload = await GetData();

        let dataDisplay = payload.map((object) => {
            const { id, imagen, descripcion, precio, stock ,caracteristicas } = object;

            return `
                <div class="stock__tarjeta__contenedor">
                    <img src="${imagen}" class="stock__tarjeta__contenedor__imagen" />

                    <div class="stock__tarjeta__contenedor__descripcion">
                        <div>${descripcion}</div>
                    </div>

                    <div class="stock__tarjeta__contenedor__precio">
                        <div>$${precio}</div>
                    </div>

                    <div class="stock__tarjeta__contenedor__stock">
                        <div>Stock: ${stock}</div>
                    </div>

                    <div class="stock__tarjeta__contenedor__caracteristicas">
                        <div class="stock__tarjeta__contenedor__caracteristicas">Características</div>
                        <ul>
                            ${caracteristicas[0] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[0]}</li>` : ""}
                            ${caracteristicas[1] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[1]}</li>` : ""}
                            ${caracteristicas[2] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[2]}</li>` : ""}
                            ${caracteristicas[3] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[3]}</li>` : ""}
                            ${caracteristicas[4] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[4]}</li>` : ""}
                        </ul>
                    </div>
                    <form action="agregarAlCarro.php" method="POST">
                        <input id="productoId" name="productoId" type="hidden" value="${id}"/>
                        <input id="productoImagen" name="productoImagen" type="hidden" value="${imagen}"/>
                        <input id="productoDescripcion" name="productoDescripcion" type="hidden" value="${descripcion}"/>
                        <input id="productoPrecio" name="productoPrecio" type="hidden" value="${precio}"/>
                        <input id="productoStock" name="productoStock" type="hidden" value="${stock}"/>
                        <input id="productoCaracteristicas" name="productoCaracteristicas_1" type="hidden" value="${caracteristicas[0]}"/>
                        <input id="productoCaracteristicas" name="productoCaracteristicas_2" type="hidden" value="${caracteristicas[1]}"/>
                        <input id="productoCaracteristicas" name="productoCaracteristicas_3" type="hidden" value="${caracteristicas[2]}"/>
                        <input id="productoCaracteristicas" name="productoCaracteristicas_4" type="hidden" value="${caracteristicas[3]}"/>
                        <input id="productoCaracteristicas" name="productoCaracteristicas_5" type="hidden" value="${caracteristicas[4]}"/>

                        <input type="number" name="productoCantidad" placeholder="Agregar al carro" />
                        <input type="submit" name="btnAgregarAlCarro" placeholder="Agregar al carro" />
                    </form>
                </div>
            `
        }).join("");
        display.innerHTML = dataDisplay;
    }

    DisplayData();

    })

    $q("#filtro3").addEventListener("click", function() {
        filtrosRef.innerHTML = "Filtrar por categoria: Juguetes";
        $q(".stock__tarjeta").innerHTML = ""

        apiFile = "../json/juguetes.json";
        const display = $q(".stock__tarjeta");
    
        const DisplayData = async () => {

            const payload = await GetData();
    
            let dataDisplay = payload.map((object) => {
                const { id, imagen, descripcion, precio, stock ,caracteristicas } = object;
    
                return `
                    <div class="stock__tarjeta__contenedor">
                        <img src="${imagen}" class="stock__tarjeta__contenedor__imagen" />
    
                        <div class="stock__tarjeta__contenedor__descripcion">
                            <div>${descripcion}</div>
                        </div>
    
                        <div class="stock__tarjeta__contenedor__precio">
                            <div>$${precio}</div>
                        </div>
    
                        <div class="stock__tarjeta__contenedor__stock">
                            <div>Stock:${stock}</div>
                        </div>
    
                        <div class="stock__tarjeta__contenedor__caracteristicas">
                            <div class="stock__tarjeta__contenedor__caracteristicas">Características</div>
                            <ul>
                                ${caracteristicas[0] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[0]}</li>` : ""}
                                ${caracteristicas[1] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[1]}</li>` : ""}
                                ${caracteristicas[2] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[2]}</li>` : ""}
                                ${caracteristicas[3] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[3]}</li>` : ""}
                                ${caracteristicas[4] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[4]}</li>` : ""}
                            </ul>
                        </div>
                        <form action="agregarAlCarro.php" method="POST">
                            <input id="productoId" name="productoId" type="hidden" value="${id}"/>
                            <input id="productoImagen" name="productoImagen" type="hidden" value="${imagen}"/>
                            <input id="productoDescripcion" name="productoDescripcion" type="hidden" value="${descripcion}"/>
                            <input id="productoPrecio" name="productoPrecio" type="hidden" value="${precio}"/>
                            <input id="productoStock" name="productoStock" type="hidden" value="${stock}"/>
                            <input id="productoCaracteristicas" name="productoCaracteristicas_1" type="hidden" value="${caracteristicas[0]}"/>
                            <input id="productoCaracteristicas" name="productoCaracteristicas_2" type="hidden" value="${caracteristicas[1]}"/>
                            <input id="productoCaracteristicas" name="productoCaracteristicas_3" type="hidden" value="${caracteristicas[2]}"/>
                            <input id="productoCaracteristicas" name="productoCaracteristicas_4" type="hidden" value="${caracteristicas[3]}"/>
                            <input id="productoCaracteristicas" name="productoCaracteristicas_5" type="hidden" value="${caracteristicas[4]}"/>
    
                            <input type="number" name="productoCantidad" placeholder="Agregar al carro" />
                            <input type="submit" name="btnAgregarAlCarro" placeholder="Agregar al carro" />
                        </form>
                    </div>
                `
            }).join("");
            display.innerHTML = dataDisplay;
        }
    
        DisplayData();    
    })

    $q("#filtro4").addEventListener("click", function() {
        filtrosRef.innerHTML = "Filtrar por categoria: Transportadoras";
        $q(".stock__tarjeta").innerHTML = ""

        apiFile = "../json/transportadoras.json";
        const display = $q(".stock__tarjeta");
    
        const DisplayData = async () => {

            const payload = await GetData();
    
            let dataDisplay = payload.map((object) => {
                const { id, imagen, descripcion, precio, stock ,caracteristicas } = object;
    
                return `
                    <div class="stock__tarjeta__contenedor">
                        <img src="${imagen}" class="stock__tarjeta__contenedor__imagen" />
    
                        <div class="stock__tarjeta__contenedor__descripcion">
                            <div>${descripcion}</div>
                        </div>
    
                        <div class="stock__tarjeta__contenedor__precio">
                            <div>$${precio}</div>
                        </div>
    
                        <div class="stock__tarjeta__contenedor__stock">
                            <div>Stock: ${stock}</div>
                        </div>
    
                        <div class="stock__tarjeta__contenedor__caracteristicas">
                            <div class="stock__tarjeta__contenedor__caracteristicas">Características</div>
                            <ul>
                                ${caracteristicas[0] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[0]}</li>` : ""}
                                ${caracteristicas[1] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[1]}</li>` : ""}
                                ${caracteristicas[2] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[2]}</li>` : ""}
                                ${caracteristicas[3] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[3]}</li>` : ""}
                                ${caracteristicas[4] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[4]}</li>` : ""}
                            </ul>
                        </div>
                        <form action="agregarAlCarro.php" method="POST">
                            <input id="productoId" name="productoId" type="hidden" value="${id}"/>
                            <input id="productoImagen" name="productoImagen" type="hidden" value="${imagen}"/>
                            <input id="productoDescripcion" name="productoDescripcion" type="hidden" value="${descripcion}"/>
                            <input id="productoPrecio" name="productoPrecio" type="hidden" value="${precio}"/>
                            <input id="productoStock" name="productoStock" type="hidden" value="${stock}"/>
                            <input id="productoCaracteristicas" name="productoCaracteristicas_1" type="hidden" value="${caracteristicas[0]}"/>
                            <input id="productoCaracteristicas" name="productoCaracteristicas_2" type="hidden" value="${caracteristicas[1]}"/>
                            <input id="productoCaracteristicas" name="productoCaracteristicas_3" type="hidden" value="${caracteristicas[2]}"/>
                            <input id="productoCaracteristicas" name="productoCaracteristicas_4" type="hidden" value="${caracteristicas[3]}"/>
                            <input id="productoCaracteristicas" name="productoCaracteristicas_5" type="hidden" value="${caracteristicas[4]}"/>
    
                            <input type="number" name="productoCantidad" placeholder="Agregar al carro" />
                            <input type="submit" name="btnAgregarAlCarro" placeholder="Agregar al carro" />
                        </form>
                    </div>
                `
            }).join("");
            display.innerHTML = dataDisplay;
        }
    
        DisplayData();    
    })

    $q("#filtro5").addEventListener("click", function() {
        filtrosRef.innerHTML = "Filtrar por categoria: Otros";
        $q(".stock__tarjeta").innerHTML = ""

        apiFile = "../json/otros.json";
        const display = $q(".stock__tarjeta");
    
        const DisplayData = async () => {

            const payload = await GetData();
    
            let dataDisplay = payload.map((object) => {
                const { id, imagen, descripcion, precio, stock ,caracteristicas } = object;
    
                return `
                    <div class="stock__tarjeta__contenedor">
                        <img src="${imagen}" class="stock__tarjeta__contenedor__imagen" />
    
                        <div class="stock__tarjeta__contenedor__descripcion">
                            <div>${descripcion}</div>
                        </div>
    
                        <div class="stock__tarjeta__contenedor__precio">
                            <div>$${precio}</div>
                        </div>
    
                        <div class="stock__tarjeta__contenedor__stock">
                            <div>Stock: ${stock}</div>
                        </div>
    
                        <div class="stock__tarjeta__contenedor__caracteristicas">
                            <div class="stock__tarjeta__contenedor__caracteristicas">Características</div>
                            <ul>
                                ${caracteristicas[0] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[0]}</li>` : ""}
                                ${caracteristicas[1] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[1]}</li>` : ""}
                                ${caracteristicas[2] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[2]}</li>` : ""}
                                ${caracteristicas[3] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[3]}</li>` : ""}
                                ${caracteristicas[4] ? `<li class="stock__tarjeta__contenedor__caracteristicas">${caracteristicas[4]}</li>` : ""}
                            </ul>
                        </div>
                        <form action="agregarAlCarro.php" method="POST">
                            <input id="productoId" name="productoId" type="hidden" value="${id}"/>
                            <input id="productoImagen" name="productoImagen" type="hidden" value="${imagen}"/>
                            <input id="productoDescripcion" name="productoDescripcion" type="hidden" value="${descripcion}"/>
                            <input id="productoPrecio" name="productoPrecio" type="hidden" value="${precio}"/>
                            <input id="productoStock" name="productoStock" type="hidden" value="${stock}"/>
                            <input id="productoCaracteristicas" name="productoCaracteristicas_1" type="hidden" value="${caracteristicas[0]}"/>
                            <input id="productoCaracteristicas" name="productoCaracteristicas_2" type="hidden" value="${caracteristicas[1]}"/>
                            <input id="productoCaracteristicas" name="productoCaracteristicas_3" type="hidden" value="${caracteristicas[2]}"/>
                            <input id="productoCaracteristicas" name="productoCaracteristicas_4" type="hidden" value="${caracteristicas[3]}"/>
                            <input id="productoCaracteristicas" name="productoCaracteristicas_5" type="hidden" value="${caracteristicas[4]}"/>
    
                            <input type="number" name="productoCantidad" placeholder="Agregar al carro" />
                            <input type="submit" name="btnAgregarAlCarro" placeholder="Agregar al carro" />
                        </form>
                    </div>
                `
            }).join("");
            display.innerHTML = dataDisplay;
        }
    
        DisplayData();    
    })
}

MostrarFiltros()

window.onload = function() {
    var boton = document.getElementById("filtro0");
    boton.click();
};