-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-10-2024 a las 02:14:07
-- Versión del servidor: 10.4.32-MariaDB
-- Versión de PHP: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `veterinaria`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carro_de_compras`
--

CREATE TABLE `carro_de_compras` (
  `id` int(255) NOT NULL,
  `id_cliente` int(255) NOT NULL,
  `producto_id` int(255) NOT NULL,
  `producto_cantidad` int(100) NOT NULL,
  `producto_imagen` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `producto_descripcion` varchar(100) NOT NULL,
  `producto_precio` double(65,2) NOT NULL,
  `producto_stock` int(200) NOT NULL,
  `producto_caracteristicas_1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `producto_caracteristicas_2` varchar(200) NOT NULL,
  `producto_caracteristicas_3` varchar(200) NOT NULL,
  `producto_caracteristicas_4` varchar(200) NOT NULL,
  `producto_caracteristicas_5` varchar(200) NOT NULL,
  `producto_precio_total` double(65,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `carro_de_compras`
--

INSERT INTO `carro_de_compras` (`id`, `id_cliente`, `producto_id`, `producto_cantidad`, `producto_imagen`, `producto_descripcion`, `producto_precio`, `producto_stock`, `producto_caracteristicas_1`, `producto_caracteristicas_2`, `producto_caracteristicas_3`, `producto_caracteristicas_4`, `producto_caracteristicas_5`, `producto_precio_total`) VALUES
(799, 0, 2, 2, '../imagenes/stock de productos/alimentos/producto 2.jpg', 'Alimento Catpro Premium Castrados/Indoor para gato adulto sabor mix en bolsa de 7.5kg', 32975.00, 1875, 'Tipo de envase: Bolsa', 'Recomendado para gato adulto.', 'Comida seca.', 'Comida seca.', 'Sabor: pollo, arroz', 98925.00),
(800, 0, 3, 3, '../imagenes/stock de productos/alimentos/producto 3.jpg', 'Purina Excellent Adult para gato adulto sabor pollo y arroz de 7.5kg', 41961.00, 1887, 'Tipo de envase: Bolsa', 'Sabor: Pollo/Arroz', 'Recomendado para gato adulto.', 'Peso de la unidad: 20 kg', 'Fragancia: Neutra', 125883.00),
(802, 3, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gatos para gato adulto sabor mix en bolsa de 10 kg', 46059.23, 1544, 'Soporta hasta un maximo de 58kg.', 'El material de la correa es poliester y el del mango es plastico.', 'Es extensible. Con boton de frenado.', 'Sabor: mix', 'Sabor y nutricion completa para tu mascota.', 46059.23);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras_realizadas`
--

CREATE TABLE `compras_realizadas` (
  `id` int(200) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `producto_id` int(50) NOT NULL,
  `producto_cantidad` int(50) NOT NULL,
  `producto_imagen` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `producto_descripcion` varchar(50) NOT NULL,
  `producto_precio` int(50) NOT NULL,
  `producto_stock` int(50) NOT NULL,
  `producto_caracteristicas_1` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `producto_caracteristicas_2` varchar(200) NOT NULL,
  `producto_caracteristicas_3` varchar(200) NOT NULL,
  `producto_caracteristicas_4` varchar(200) NOT NULL,
  `producto_caracteristicas_5` varchar(200) NOT NULL,
  `producto_precio_total` int(50) NOT NULL,
  `fecha_de_compra` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `compras_realizadas`
--

INSERT INTO `compras_realizadas` (`id`, `id_cliente`, `producto_id`, `producto_cantidad`, `producto_imagen`, `producto_descripcion`, `producto_precio`, `producto_stock`, `producto_caracteristicas_1`, `producto_caracteristicas_2`, `producto_caracteristicas_3`, `producto_caracteristicas_4`, `producto_caracteristicas_5`, `producto_precio_total`, `fecha_de_compra`) VALUES
(117, 0, 5, 1, '../imagenes/stock de productos/otros/producto 5.jpg', 'Piedra Rubicat Classic Arena Aglomerante Pack Ahor', 9990, 1399, 'Marca: Rubicat Classic,Formato de venta: Unidad,Peso neto: 20 kg,Peso de la unidad: 20 kg,Fragancia: Neutra', '', '', '', '', 9990, '2024-10-09 02:07:40'),
(120, 0, 2, 1, '../imagenes/stock de productos/alimentos/producto 2.jpg', 'Alimento Catpro Premium Castrados/Indoor para gato', 32975, 1875, 'Tipo de envase: Bolsa', 'Recomendado para gato adulto.', 'Comida seca.', 'Sabor: mix', 'Sabor y nutrición completa para tu mascota.', 32975, '2024-10-09 02:37:14'),
(121, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1556, 'Soporta hasta un máximo de 58kg.', 'El material de la correa es poliéster y el del mango es plástico.', 'Es extensible. Con botón de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 03:28:49'),
(122, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1556, 'Soporta hasta un máximo de 58kg.', 'El material de la correa es poliéster y el del mango es plástico.', 'Es extensible. Con botón de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 03:30:29'),
(123, 0, 1, 2, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1556, 'Soporta hasta un máximo de 58kg.', 'El material de la correa es poliéster y el del mango es plástico.', 'Es extensible. Con botón de frenado.', 'undefined', 'undefined', 92118, '2024-10-09 03:32:24'),
(124, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1556, 'Soporta hasta un máximo de 58kg.', 'El material de la correa es poliéster y el del mango es plástico.', 'Es extensible. Con botón de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 03:39:18'),
(125, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1556, 'Soporta hasta un máximo de 58kg.', 'El material de la correa es poliéster y el del mango es plástico.', 'Es extensible. Con botón de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 03:43:36'),
(126, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1556, 'Soporta hasta un máximo de 58kg.', 'El material de la correa es poliéster y el del mango es plástico.', 'Es extensible. Con botón de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 03:45:00'),
(127, 0, 1, 3, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1556, 'Soporta hasta un máximo de 58kg.', 'El material de la correa es poliéster y el del mango es plástico.', 'Es extensible. Con botón de frenado.', 'undefined', 'undefined', 138178, '2024-10-09 03:49:41'),
(128, 0, 2, 1, '../imagenes/stock de productos/alimentos/producto 2.jpg', 'Alimento Catpro Premium Castrados/Indoor para gato', 32975, 1875, 'Tipo de envase: Bolsa', 'Recomendado para gato adulto.', 'Comida seca.', 'Sabor: mix', 'Sabor y nutrición completa para tu mascota.', 32975, '2024-10-09 03:52:33'),
(129, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1556, 'Soporta hasta un máximo de 58kg.', 'El material de la correa es poliéster y el del mango es plástico.', 'Es extensible. Con botón de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 03:53:32'),
(130, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1556, 'Soporta hasta un máximo de 58kg.', 'El material de la correa es poliéster y el del mango es plástico.', 'Es extensible. Con botón de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 03:58:31'),
(131, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1556, 'Soporta hasta un máximo de 58kg.', 'El material de la correa es poliéster y el del mango es plástico.', 'Es extensible. Con botón de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 03:59:06'),
(132, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1556, 'Soporta hasta un maximo de 58kg.', 'El material de la correa es poliester y el del mango es plastico.', 'Es extensible. Con boton de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 04:04:14'),
(133, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1556, 'Soporta hasta un maximo de 58kg.', 'El material de la correa es poliester y el del mango es plastico.', 'Es extensible. Con boton de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 04:11:09'),
(134, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1556, 'Soporta hasta un maximo de 58kg.', 'El material de la correa es poliester y el del mango es plastico.', 'Es extensible. Con boton de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 04:14:08'),
(135, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1556, 'Soporta hasta un maximo de 58kg.', 'El material de la correa es poliester y el del mango es plastico.', 'Es extensible. Con boton de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 04:18:37'),
(136, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1555, 'Soporta hasta un maximo de 58kg.', 'El material de la correa es poliester y el del mango es plastico.', 'Es extensible. Con boton de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 04:24:10'),
(137, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1554, 'Soporta hasta un maximo de 58kg.', 'El material de la correa es poliester y el del mango es plastico.', 'Es extensible. Con boton de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 04:28:02'),
(138, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1553, 'Soporta hasta un maximo de 58kg.', 'El material de la correa es poliester y el del mango es plastico.', 'Es extensible. Con boton de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 04:34:34'),
(139, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1552, 'Soporta hasta un maximo de 58kg.', 'El material de la correa es poliester y el del mango es plastico.', 'Es extensible. Con boton de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 04:37:12'),
(140, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1551, 'Soporta hasta un maximo de 58kg.', 'El material de la correa es poliester y el del mango es plastico.', 'Es extensible. Con boton de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 04:41:19'),
(141, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1550, 'Soporta hasta un maximo de 58kg.', 'El material de la correa es poliester y el del mango es plastico.', 'Es extensible. Con boton de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 04:43:28'),
(142, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1549, 'Soporta hasta un maximo de 58kg.', 'El material de la correa es poliester y el del mango es plastico.', 'Es extensible. Con boton de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 04:46:41'),
(143, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1549, 'Soporta hasta un maximo de 58kg.', 'El material de la correa es poliester y el del mango es plastico.', 'Es extensible. Con boton de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 04:47:40'),
(144, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1549, 'Soporta hasta un maximo de 58kg.', 'El material de la correa es poliester y el del mango es plastico.', 'Es extensible. Con boton de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 04:48:29'),
(145, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1548, 'Soporta hasta un maximo de 58kg.', 'El material de la correa es poliester y el del mango es plastico.', 'Es extensible. Con boton de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 04:50:19'),
(146, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1548, 'Soporta hasta un maximo de 58kg.', 'El material de la correa es poliester y el del mango es plastico.', 'Es extensible. Con boton de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 04:51:11'),
(147, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1548, 'Soporta hasta un maximo de 58kg.', 'El material de la correa es poliester y el del mango es plastico.', 'Es extensible. Con boton de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 04:52:10'),
(148, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1548, 'Soporta hasta un maximo de 58kg.', 'El material de la correa es poliester y el del mango es plastico.', 'Es extensible. Con boton de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 04:58:47'),
(149, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1546, 'Soporta hasta un maximo de 58kg.', 'El material de la correa es poliester y el del mango es plastico.', 'Es extensible. Con boton de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 04:59:22'),
(151, 0, 1, 1, '../imagenes/stock de productos/alimentos/producto 1.jpg', 'Alimento Agility Premium Agility Urinary para gato', 46059, 1545, 'Soporta hasta un maximo de 58kg.', 'El material de la correa es poliester y el del mango es plastico.', 'Es extensible. Con boton de frenado.', 'undefined', 'undefined', 46059, '2024-10-09 05:04:54'),
(152, 0, 11, 1, '../imagenes/stock de productos/camas/cucha gato.jpg', 'Cucha para gato redondo chico', 8300, 3333, '', '', '', '', '', 8300, '2024-10-09 05:13:39'),
(153, 0, 11, 1, '../imagenes/stock de productos/camas/cucha gato.jpg', 'Cucha para gato redondo chico', 8300, 3332, '', '', '', '', '', 8300, '2024-10-09 05:14:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto`
--

CREATE TABLE `contacto` (
  `Id` int(255) NOT NULL,
  `Id_cliente` int(255) NOT NULL,
  `Fecha` datetime NOT NULL,
  `Nombre` text NOT NULL,
  `Correo_electronico` varchar(50) NOT NULL,
  `Asunto` varchar(50) NOT NULL,
  `Comentarios` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mascotas`
--

CREATE TABLE `mascotas` (
  `Id` int(100) NOT NULL,
  `Nombre` varchar(30) NOT NULL,
  `Raza` varchar(30) NOT NULL,
  `Sexo` varchar(10) NOT NULL,
  `Edad` varchar(10) NOT NULL,
  `Nro_de_microchip` varchar(30) NOT NULL,
  `Vacuna_antirrábica` varchar(50) NOT NULL,
  `Tratamiento_antiparasitario` varchar(50) NOT NULL,
  `Otras_vacunas` varchar(50) NOT NULL,
  `Información_adicional` varchar(50) NOT NULL,
  `Id_cliente` int(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `mascotas`
--

INSERT INTO `mascotas` (`Id`, `Nombre`, `Raza`, `Sexo`, `Edad`, `Nro_de_microchip`, `Vacuna_antirrábica`, `Tratamiento_antiparasitario`, `Otras_vacunas`, `Información_adicional`, `Id_cliente`) VALUES
(5, 'Bethoven', 'Bulldog', 'macho', '5', '1', 'no', 'no', 'no', 'no', 3),
(6, 'dd', 'dd', 'dd', '1', '1', 'd', 'd', 'd', 'd', 3),
(11, 'ff', 'fff', 'fff', '22', '22', 'fff', 'fff', 'fff', 'fff', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `turnos`
--

CREATE TABLE `turnos` (
  `Id` int(255) NOT NULL,
  `Id_cliente` int(255) NOT NULL,
  `Fecha` datetime NOT NULL,
  `Mascota_a_atender` varchar(50) NOT NULL,
  `Asunto_a_atender` varchar(255) NOT NULL,
  `Mensaje` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `turnos`
--

INSERT INTO `turnos` (`Id`, `Id_cliente`, `Fecha`, `Mascota_a_atender`, `Asunto_a_atender`, `Mensaje`) VALUES
(10, 3, '2024-06-28 21:48:00', 'Bethoven', 'a', 'a'),
(11, 3, '2024-10-24 20:48:00', 'Bethoven', 'r', 'r'),
(12, 3, '2024-06-07 21:14:00', 'Bethoven', 'asunto10', 'mensaje10'),
(13, 3, '2024-06-07 23:18:00', 'Bethoven', 'hhhh', 'hhhhh');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `Id` int(100) NOT NULL,
  `Nombre` varchar(30) NOT NULL,
  `Apellido` varchar(30) NOT NULL,
  `DNI` varchar(30) NOT NULL,
  `CUIL_CUIT` varchar(30) NOT NULL,
  `Dirección` varchar(30) NOT NULL,
  `Piso` varchar(10) NOT NULL,
  `Departamento` varchar(10) NOT NULL,
  `Localidad` varchar(30) NOT NULL,
  `Teléfono` varchar(30) NOT NULL,
  `Celular` varchar(30) NOT NULL,
  `Email` varchar(30) NOT NULL,
  `Usuario` varchar(30) NOT NULL,
  `Contraseña` varchar(30) NOT NULL,
  `administrador` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`Id`, `Nombre`, `Apellido`, `DNI`, `CUIL_CUIT`, `Dirección`, `Piso`, `Departamento`, `Localidad`, `Teléfono`, `Celular`, `Email`, `Usuario`, `Contraseña`, `administrador`) VALUES
(0, 'g', 'g', '37456948', '23374569483', 'g', '3', 'g', 'g', '3', '3', 'sedent333@gmail.com', '123', '123', 1),
(3, 'gggg', 'gggg', '333', '333', 'ggg', '33', 'gg', 'gggg', '333', '333', 'gggg@gmail.com', 'asd', 'asd', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `carro_de_compras`
--
ALTER TABLE `carro_de_compras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cliente` (`id_cliente`),
  ADD KEY `producto_id` (`producto_id`),
  ADD KEY `producto_cantidad` (`producto_cantidad`),
  ADD KEY `producto_imagen` (`producto_imagen`),
  ADD KEY `producto_descripcion` (`producto_descripcion`),
  ADD KEY `producto_precio` (`producto_precio`),
  ADD KEY `producto_stock` (`producto_stock`),
  ADD KEY `producto_caracteristicas_1` (`producto_caracteristicas_1`),
  ADD KEY `producto_caracteristicas_2` (`producto_caracteristicas_2`),
  ADD KEY `producto_caracteristicas_3` (`producto_caracteristicas_3`),
  ADD KEY `producto_caracteristicas_4` (`producto_caracteristicas_4`),
  ADD KEY `producto_caracteristicas_5` (`producto_caracteristicas_5`),
  ADD KEY `producto_precio_total` (`producto_precio_total`);

--
-- Indices de la tabla `compras_realizadas`
--
ALTER TABLE `compras_realizadas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cliente` (`id_cliente`),
  ADD KEY `producto_id` (`producto_id`),
  ADD KEY `producto_cantidad` (`producto_cantidad`),
  ADD KEY `producto_imagen` (`producto_imagen`),
  ADD KEY `producto_descripcion` (`producto_descripcion`),
  ADD KEY `producto_precio` (`producto_precio`),
  ADD KEY `producto_stock` (`producto_stock`),
  ADD KEY `producto_caracteristicas_1` (`producto_caracteristicas_1`),
  ADD KEY `producto_caracteristicas_2` (`producto_caracteristicas_2`),
  ADD KEY `producto_caracteristicas_3` (`producto_caracteristicas_3`),
  ADD KEY `producto_caracteristicas_4` (`producto_caracteristicas_4`),
  ADD KEY `producto_caracteristicas_5` (`producto_caracteristicas_5`),
  ADD KEY `producto_precio_total` (`producto_precio_total`);

--
-- Indices de la tabla `contacto`
--
ALTER TABLE `contacto`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Id_cliente` (`Id_cliente`),
  ADD KEY `Correo_electronico` (`Correo_electronico`);

--
-- Indices de la tabla `mascotas`
--
ALTER TABLE `mascotas`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Id_cliente` (`Id_cliente`);

--
-- Indices de la tabla `turnos`
--
ALTER TABLE `turnos`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Id_cliente` (`Id_cliente`),
  ADD KEY `Mascota_a_atender` (`Mascota_a_atender`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `carro_de_compras`
--
ALTER TABLE `carro_de_compras`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=803;

--
-- AUTO_INCREMENT de la tabla `compras_realizadas`
--
ALTER TABLE `compras_realizadas`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;

--
-- AUTO_INCREMENT de la tabla `contacto`
--
ALTER TABLE `contacto`
  MODIFY `Id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mascotas`
--
ALTER TABLE `mascotas`
  MODIFY `Id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `turnos`
--
ALTER TABLE `turnos`
  MODIFY `Id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `Id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `carro_de_compras`
--
ALTER TABLE `carro_de_compras`
  ADD CONSTRAINT `carro_de_compras_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `usuarios` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `compras_realizadas`
--
ALTER TABLE `compras_realizadas`
  ADD CONSTRAINT `compras_realizadas_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `usuarios` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `contacto`
--
ALTER TABLE `contacto`
  ADD CONSTRAINT `contacto_ibfk_1` FOREIGN KEY (`Id_cliente`) REFERENCES `usuarios` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `turnos`
--
ALTER TABLE `turnos`
  ADD CONSTRAINT `turnos_ibfk_1` FOREIGN KEY (`Id_cliente`) REFERENCES `usuarios` (`Id`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
