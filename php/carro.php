<?php
session_start();
if (isset($_SESSION["sessionUsuario"])) {
        echo '';
    }
    else 
    {
        $_SESSION["sessionUsuario"] = "";
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../main.css">
    <script src="../componentes js/variablesGlobales.js" defer></script>
    <script src="../componentes js/header.js" defer></script>
    <script src="https://www.paypal.com/sdk/js?client-id=AfLxVrsnl9NTsc5h92OORBnWTMF3-EFJUU1Pc5Xi8qBamzdqDb0LRpowwaYcPBzAEWJOidUSNpN1Bf9W&currency=USD"></script>
    <script src="../componentes js/paypal.js" defer></script>
    <title>Veterinaria</title>
</head>
<body>
    <header class="header">
        <?php 
            if($_SESSION["sessionUsuario"] == "") {
            echo '
                <nav>
                    <ul>
                        <li class="header__logoContenedor"><img src="../imagenes/logo.png" alt="logo" class="header__logoContenedor__logo"></li>
                        <li class="header__inicio"><a href="./index.php">Inicio</a></li>
                        <li class="header__acercaDeNosotros"><a href="./acercaDeNosotros.php">Acerca de Nosotros</a></li>
                        <li class="header__contacto"><a href="./contacto.php" class="header__contacto__button">Contacto</a></li>
                        <li class="header__logIn"><a href="./logIn.php" class="header__logIn__button">Log In</a></li> 
                        <button class="header__navBar" title="navBarMenu">
                            <div class="header__navBar__line"></div>
                            <div class="header__navBar__line"></div>
                        </button>
                    </ul>
                </nav>
                ';}
            else {
                echo '
                '.$_SESSION["cantidadDeProductosEnCarro"] == 0 ? '
                <nav>
                    <ul>
                        <li class="header__logoContenedor"><img src="../imagenes/logo.png" alt="logo" class="header__logoContenedor__logo"></li>
                        <li class="header__inicio"><a href="./index.php">Inicio</a></li>
                        <li class="header__acercaDeNosotros"><a href="./acercaDeNosotros.php">Acerca de Nosotros</a></li>
                        <li class="header__contacto"><a href="./contacto.php" class="header__contacto__button">Contacto</a></li>
                        <li class="header__logIn"><a href="LogOut.php" class="header__logOut__button">Log Out</a></li>
                        <button class="header__navBar" title="navBarMenu">
                            <div class="header__navBar__line"></div>
                            <div class="header__navBar__line"></div>
                        </button>
                        <li class="header__perfil"><img src="../imagenes/iconos/usuario.svg"/ class="header__iconoUsuario /><div">'.$_SESSION["sessionUsuario"].'</div></li>
                    </ul>
                </nav>' 
                : 
                '
                <nav>
                    <ul>
                        <li class="header__logoContenedor"><img src="../imagenes/logo.png" alt="logo" class="header__logoContenedor__logo"></li>
                        <li class="header__inicio"><a href="./index.php">Inicio</a></li>
                        <li class="header__acercaDeNosotros"><a href="./acercaDeNosotros.php">Acerca de Nosotros</a></li>
                        <li class="header__contacto"><a href="./contacto.php" class="header__contacto__button">Contacto</a></li>
                        <li class="header__logIn"><a href="LogOut.php" class="header__logOut__button">Log Out</a></li>
                        <button class="header__navBar" title="navBarMenu">
                            <div class="header__navBar__line"></div>
                            <div class="header__navBar__line"></div>
                        </button>
                        <li class="header__perfil"><img src="../imagenes/iconos/usuario.svg"/ class="header__iconoUsuario /><div">'.$_SESSION["sessionUsuario"].'</div></li>
                        <li class="header__carroDeCompra">
                            <a href="./carro.php">
                                <img src="../imagenes/iconos/shopping-cart.svg" class="header__carroDeCompra__button" />
                                <span class="header__carroDeCompra__cantidad">'.$_SESSION["cantidadDeProductosEnCarro"].'</span>
                            </a>
                        </li>
                    </ul>
                </nav>' ;
            }
        ?>
    </header>

    <div class="navBarMenu">
        <?php 
            if($_SESSION["sessionUsuario"] == "") {
            echo '
                <a href="./index.php" class="navBarMenu__home">Inicio</a>
                <a href="./acercaDeNosotros.php" class="navBarMenu__projects">Acerca de Nosotros</a>
                <a href="./contacto.php" class="navBarMenu__contacto">Contacto</a>
                <a href="./logIn.php" class="navBarMenu__logIn">Log In</a>
                <a href="./turnos.php" class="navBarMenu__turnos">Solicitar Turno</a>
                <a href="./stock.php" class="navBarMenu__stock">Ver Productos</a>
            ';}
            else
            {
                echo '
                    <a href="./index.php" class="navBarMenu__home">Inicio</a>
                    <a href="./acercaDeNosotros.php" class="navBarMenu__projects">Acerca de Nosotros</a>
                    <a href="./contacto.php" class="navBarMenu__contacto">Contacto</a>
                    <a href="./formulario_perfil.php" class="navBarMenu__formularioPerfil">Editar perfil</a>
                    <a href="./formulario_mascota.php" class="navBarMenu__formularioMascota">Editar mascota</a>
                    <a href="./turnos.php" class="navBarMenu__turnos">Solicitar Turno</a>
                    <a href="./stock.php" class="navBarMenu__stock">Ver Productos</a>
                ';
            }
        ?>
    </div>

    <section class="carro seccion">

    <a href="https://api.whatsapp.com/send?phone=+5491155912380&text=Hola! Quisiera más información!" target="_blank" >
    <img src="../imagenes/iconos/whatsapp2.png" class="globalWhatsapp" /></a>


        <?php 
            include("conexion.php");
            $idUsuarioActual = $_SESSION["sessionId"];

            $idUsuarioActual = $_SESSION["sessionId"];
            $result = mysqli_query($connect, " SELECT * FROM carro_de_compras WHERE id_cliente = '$idUsuarioActual' ");

            while ($row = mysqli_fetch_array($result)) {
                $number = $row['producto_precio'];
                
                // Inicia el echo y guarda en una variable
                $output = '
                <div class="carro__tarjetaCarro__contenedor">
                    <img src="' . $row['producto_imagen'] . '" class="carro__tarjetaCarro__contenedor__imagen" />
                    
                    <div class="carro__tarjetaCarro__contenedor__descripcion">
                        <div>' . $row['producto_descripcion'] . '</div>
                    </div>
                    
                    <div class="carro__tarjetaCarro__contenedor__precio">
                        <div>$' . number_format($number, 2, ',', '.') . '</div>
                    </div>
                    
                    <div class="carro__tarjetaCarro__contenedor__stock">';
            
                // Mostrar stock solo si es administrador
                if (isset($_SESSION["esAdministrador"]) && $_SESSION["esAdministrador"] == 1) {
                    $output .= '<div>Stock: ' . $row['producto_stock'] . '</div>'; // Usa concatenación para añadir stock
                } 
            
                // Continúa con el resto del contenido HTML
                $output .= '
                    </div> <!-- Fin de carro__tarjetaCarro__contenedor__stock -->
                    
                    <div class="carro__tarjetaCarro__contenedor__caracteristicas">
                        <div class="carro__tarjetaCarro__contenedor__caracteristicas__titulo">Características</div>
                        <ul>
                            <li class="carro__tarjetaCarro__contenedor__caracteristicas__contenido">' . $row['producto_caracteristicas_1'] . '</li>
                        </ul>
                        <ul>
                            <li class="carro__tarjetaCarro__contenedor__caracteristicas__contenido">' . $row['producto_caracteristicas_2'] . '</li>
                        </ul>
                        <ul>
                            <li class="carro__tarjetaCarro__contenedor__caracteristicas__contenido">' . $row['producto_caracteristicas_3'] . '</li>
                        </ul>
                        <ul>
                            <li class="carro__tarjetaCarro__contenedor__caracteristicas__contenido">' . $row['producto_caracteristicas_4'] . '</li>
                        </ul>
                        <ul>
                            <li class="carro__tarjetaCarro__contenedor__caracteristicas__contenido">' . $row['producto_caracteristicas_5'] . '</li>
                        </ul>
                    </div>
                    <div class="carro__tarjetaCarro__contenedor__cantidad">
                        <div class="carro__tarjetaCarro__contenedor__cantidad">Cantidad: ' . $row['producto_cantidad'] . '</div>
                    </div>
                    <form action="borrarDelCarro.php" method="POST">
                        <input id="productId" name="productId" type="hidden" value="' . $row['producto_id'] . '"/>
                        <input id="imagenId" name="imagenId" type="hidden" value="' . $row['producto_imagen'] . '"/>
                        <input id="descripcionId" name="descripcionId" type="hidden" value="' . $row['producto_descripcion'] . '"/>
                        <input id="precioId" name="precioId" type="hidden" value="' . $row['producto_precio'] . '"/>
                        <input id="stockId" name="stockId" type="hidden" value="' . $row['producto_stock'] . '"/>
                        <input id="stockCaracteristicas" name="stockCaracteristicas_1" type="hidden" value="' . $row['producto_caracteristicas_1'] . '"/>
                        <input id="stockCaracteristicas" name="stockCaracteristicas_2" type="hidden" value="' . $row['producto_caracteristicas_2'] . '"/>
                        <input id="stockCaracteristicas" name="stockCaracteristicas_3" type="hidden" value="' . $row['producto_caracteristicas_3'] . '"/>
                        <input id="stockCaracteristicas" name="stockCaracteristicas_4" type="hidden" value="' . $row['producto_caracteristicas_4'] . '"/>
                        <input id="stockCaracteristicas" name="stockCaracteristicas_5" type="hidden" value="' . $row['producto_caracteristicas_5'] . '"/>
                    
                        <input type="submit" name="btnBorrarDelCarro" placeholder="Remover del carro" value="Remover del carro" />
                    </form>
                </div>';
            
                // Imprimir el resultado
                echo $output;
            }
            
            $suma = mysqli_query($connect, "SELECT SUM(producto_precio_total) as total FROM carro_de_compras WHERE id_cliente = '$idUsuarioActual'");
            $rowSuma = mysqli_fetch_array($suma);
            $sumaResultado = $rowSuma['total'];
            echo'
                <div class="carro__resultadoCompra">
                    <div data-value="'.$sumaResultado.'" id="amount">Coste Total de compra: '.$sumaResultado.'</div>
                        <div id="paypal-button-container"></div>
                    <script>
                        paypal.buttons({
                            style: {
                                layout: "vertical",
                                color: "blue",
                                shape: "rect",
                                label: "paypal"
                            }
                        }).render("#paypal-button-container")
                    </script>
                </div>
            ';

        ?>


    </section>

    <footer>
        <div>Términos de uso</div>
        <div>© 1996-1997 +cota</div>
        <div>Politicas de privacidad</div>
    </footer>
</body>
</html>