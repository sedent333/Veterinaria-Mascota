<?php
    session_start();
    if (isset($_SESSION["sessionUsuario"])) {
            echo '';
        }
        else 
        {
            $_SESSION["sessionUsuario"] = "";
    }

    if (isset($_SESSION["nombreMascota"])) {
        echo '';
    }
    else 
    {
        $_SESSION["nombreMascota"] = "";
    }
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>ABM de Mascotas</title>
    <link rel="stylesheet" href="../main.css">
    <script src="../componentes js/variablesGlobales.js" defer></script>
    <script src="../componentes js/header.js" defer></script>
</head>
<body>
    <header class="header">
        <?php 
            if($_SESSION["sessionUsuario"] == "") {
            echo '
                <nav>
                    <ul>
                        <li class="header__logoContenedor"><img src="../imagenes/logo.png" alt="logo" class="header__logoContenedor__logo"></li>
                        <li class="header__inicio"><a href="./index.php">Inicio</a></li>
                        <li class="header__acercaDeNosotros"><a href="./acercaDeNosotros.php">Acerca de Nosotros</a></li>
                        <li class="header__contacto"><a href="./contacto.php" class="header__contacto__button">Contacto</a></li>
                        <li class="header__logIn"><a href="./logIn.php" class="header__logIn__button">Log In</a></li> 
                        <button class="header__navBar" title="navBarMenu">
                            <div class="header__navBar__line"></div>
                            <div class="header__navBar__line"></div>
                        </button>
                    </ul>
                </nav>
                ';}
            else {
                echo '
                '.$_SESSION["cantidadDeProductosEnCarro"] == 0 ? '
                <nav>
                    <ul>
                        <li class="header__logoContenedor"><img src="../imagenes/logo.png" alt="logo" class="header__logoContenedor__logo"></li>
                        <li class="header__inicio"><a href="./index.php">Inicio</a></li>
                        <li class="header__acercaDeNosotros"><a href="./acercaDeNosotros.php">Acerca de Nosotros</a></li>
                        <li class="header__contacto"><a href="./contacto.php" class="header__contacto__button">Contacto</a></li>
                        <li class="header__logIn"><a href="LogOut.php" class="header__logOut__button">Log Out</a></li>
                        <button class="header__navBar" title="navBarMenu">
                            <div class="header__navBar__line"></div>
                            <div class="header__navBar__line"></div>
                        </button>
                        <li class="header__perfil"><img src="../imagenes/iconos/usuario.svg"/ class="header__iconoUsuario /><div">'.$_SESSION["sessionUsuario"].'</div></li>
                    </ul>
                </nav>' 
                : 
                '
                <nav>
                    <ul>
                        <li class="header__logoContenedor"><img src="../imagenes/logo.png" alt="logo" class="header__logoContenedor__logo"></li>
                        <li class="header__inicio"><a href="./index.php">Inicio</a></li>
                        <li class="header__acercaDeNosotros"><a href="./acercaDeNosotros.php">Acerca de Nosotros</a></li>
                        <li class="header__contacto"><a href="./contacto.php" class="header__contacto__button">Contacto</a></li>
                        <li class="header__logIn"><a href="LogOut.php" class="header__logOut__button">Log Out</a></li>
                        <button class="header__navBar" title="navBarMenu">
                            <div class="header__navBar__line"></div>
                            <div class="header__navBar__line"></div>
                        </button>
                        <li class="header__perfil"><img src="../imagenes/iconos/usuario.svg"/ class="header__iconoUsuario /><div">'.$_SESSION["sessionUsuario"].'</div></li>
                        <li class="header__carroDeCompra">
                            <a href="./carro.php">
                                <img src="../imagenes/iconos/shopping-cart.svg" class="header__carroDeCompra__button" />
                                <span class="header__carroDeCompra__cantidad">'.$_SESSION["cantidadDeProductosEnCarro"].'</span>
                            </a>
                        </li>
                    </ul>
                </nav>' ;
            }
        ?>
    </header>

    <div class="navBarMenu">
        <?php 
            if($_SESSION["sessionUsuario"] == "") {
            echo '
                <a href="./index.php" class="navBarMenu__home">Inicio</a>
                <a href="./acercaDeNosotros.php" class="navBarMenu__projects">Acerca de Nosotros</a>
                <a href="./contacto.php" class="navBarMenu__contacto">Contacto</a>
                <a href="./logIn.php" class="navBarMenu__logIn">Log In</a>
                <a href="./turnos.php" class="navBarMenu__turnos">Solicitar Turno</a>
                <a href="./stock.php" class="navBarMenu__stock">Ver Productos</a>
            ';}
            else
            {
                echo '
                    <a href="./index.php" class="navBarMenu__home">Inicio</a>
                    <a href="./acercaDeNosotros.php" class="navBarMenu__projects">Acerca de Nosotros</a>
                    <a href="./contacto.php" class="navBarMenu__contacto">Contacto</a>
                    <a href="./formulario_perfil.php" class="navBarMenu__formularioPerfil">Editar perfil</a>
                    <a href="./formulario_mascota.php" class="navBarMenu__formularioMascota">Editar mascota</a>
                    <a href="./turnos.php" class="navBarMenu__turnos">Solicitar Turno</a>
                    <a href="./compras_realizadas.php" class="navBarMenu__formularioMascota">Compras realizadas</a>
                    <a href="./stock.php" class="navBarMenu__stock">Ver Productos</a>
                ';
            }
        ?>
    </div>
    <a href="https://api.whatsapp.com/send?phone=+5491155912380&text=Hola! Quisiera más información!" target="_blank" >
    <img src="../imagenes/iconos/whatsapp2.png" class="globalWhatsapp" /></a>

    <section class="editarMascota">
        <div class="contenedor-formularios">
            <ul class="contenedor-tabs" style="margin:0">
                <li class="tab tab-primera active"><a href="#agregar-mascota">Agregar Mascota</a></li>
                <li class="tab tab-segunda"><a href="#editar-mascota">Editar Mascota</a></li>
                <li class="tab tab-tercera"><a href="#ver-mascota">Ver Mascota</a></li>
                <li class="tab tab-cuarta"><a href="#borrar-mascota">Borrar Mascota</a></li>
            </ul>

            <div class="contenido-tab">   
                <div id="agregar-mascota">
                    <h1 style="padding:10px 0">Agregar Mascota</h1>
                    <form action="agregar_mascota.php" method="post" >
                        
                        <div class="contenedor-input">
                            <label>Nombre de la mascota<span class="req">*</span></label>
                            <input type="text" name="Nombre" required >
                        </div>
                        <br>
                        <div class="fila-arriba">
                            <div class="contenedor-input">
                                <label>Raza<span class="req">*</span></label>
                                <input type="text" name="Raza" required>
                            </div>
                            <div class="contenedor-input">
                                <label>Sexo<span class="req">*</span></label>
                                <input type="text" name="Sexo" required>
                            </div>
                        </div>
                        <br>
                        <div class="fila-arriba">
                            <div class="contenedor-input">
                                <label>Edad<span class="req">*</span></label>
                                <input type="number" name="Edad" required>
                            </div>
                            <div class="contenedor-input">
                                <label>Nro. de microchip<span class="req"></span></label>
                                <input type="text" name="Nro_de_microchip">
                            </div>
                        </div>
                        <br>
                        <div class="contenedor-input">
                            <label>Vacuna antirrábica<span class="req">*</span></label>
                            <input type="text" name="Vacuna_antirrábica" required>
                        </div>
                        <br>
                        <div class="contenedor-input">
                            <label>Tratamiento antiparasitario<span class="req"></span></label>
                            <input type="text" name="Tratamiento_antiparasitario">
                        </div>
                        <br>
                        <div class="contenedor-input">
                            <label>Otras vacunas<span class="req">*</span></label>
                            <input type="text" name="Otras_vacunas" required>
                        </div>
                        <br>
                        <div class="contenedor-input">
                            <label>Información adicional<span class="req"></span></label>    
                            <input type="text" name="Información_adicional">
                        </div>
                        <br>
                        <input type="submit" class="button button-block" value="Agregar mascota">
                    </form>
                </div>

                <div id="editar-mascota" style="display: none;">
                    <form action="editar_mascota.php" method="post" >
                        <?php 
                            include("conexion.php");
                        ?>
                        <h1 style="padding:10px 0">Editar Mascota</h1>
                        <div class="contenedor-input">
                            <p style="color:white; padding:10px 0">Elegir mascota</p>
                            <select id="NombreMascotaElegida" name="NombreMascotaElegida" style="height:40px; min-width:100%">
                                <?php 
                                    include("conexion.php");
                                    $result = mysqli_query($connect, " select * from mascotas where Id_cliente='".$_SESSION['sessionId']."'");
                                    $i = 0;
                                    while($row = mysqli_fetch_array($result)){
                                    echo '            
                                        <option value="'.$row['Nombre'].'">'.$row['Nombre'].'</option>                  
                                    ';
                                    $i++;}
                                ?>
                            </select>
                        </div>
                        <div class="contenedor-input">
                            <label>Nombre<span class="req">*</span></label>
                            <input type="text" name="Nombre" required >
                        </div>
                        <br>
                        <div class="fila-arriba">
                            <div class="contenedor-input">
                                <label>Raza<span class="req">*</span></label>
                                <input type="text" name="Raza" required>
                            </div>
                            <div class="contenedor-input">
                                <label>Sexo<span class="req">*</span></label>
                                <input type="text" name="Sexo" required>
                            </div>
                        </div>
                        <br>
                        <div class="fila-arriba">
                            <div class="contenedor-input">
                                <label>Edad<span class="req">*</span></label>
                                <input type="number" name="Edad" required>
                            </div>
                            <div class="contenedor-input">
                                <label>Nro. de microchip<span class="req"></span></label>
                                <input type="text" name="Nro_de_microchip">
                            </div>
                        </div>
                        <br>
                        <div class="contenedor-input">
                            <label>Vacuna antirrábica<span class="req">*</span></label>
                            <input type="text" name="Vacuna_antirrábica" required>
                        </div>
                        <br>
                        <div class="contenedor-input">
                            <label>Tratamiento antiparasitario<span class="req"></span></label>
                            <input type="text" name="Tratamiento_antiparasitario">
                        </div>
                        <br>
                        <div class="contenedor-input">
                            <label>Otras vacunas<span class="req">*</span></label>
                            <input type="text" name="Otras_vacunas" required>
                        </div>
                        <br>
                        <div class="contenedor-input">
                            <label>Información adicional<span class="req"></span></label>    
                            <input type="text" name="Información_adicional">
                        </div>
                        <input type="submit" class="button button-block" value="Editar mascota">
                    </form>
                </div>

                <div id="ver-mascota" style="display: none;">
                    <?php 
                        include("conexion.php");
                    ?>
                    <form action="ver_mascota.php" method="POST">
                        <h1 style="color:white; padding:10px 0">Ver mascota</h1>
                        <p style="color:white; padding:10px 0">Elegir mascota</p>
                        <select id="NombreMascotaElegida" name="NombreMascotaElegida" style="height:40px; min-width:100%">
                            <?php 
                                include("conexion.php");
                                $result = mysqli_query($connect, " select * from mascotas where Id_cliente='".$_SESSION['sessionId']."'");
                                $i = 0;
                                while($row = mysqli_fetch_array($result)){
                                echo '            
                                    <option value="'.$row['Nombre'].'">'.$row['Nombre'].'</option>                  
                                ';
                                $i++;}
                            ?>
                        </select>
                        <input type="submit" class="button button-block" value="Ver mascota">
                    </form>
                </div>

                <div id="borrar-mascota" style="display: none;">
                    <form action="borrar_mascota.php" method="POST">
                        <h1 style="color:white; padding:10px 0">Borrar mascota</h1>
                        <p style="color:white; padding:10px 0">Elegir mascota</p>
                        <select id="NombreMascotaElegida" name="NombreMascotaElegida" style="height:40px; min-width:100%">
                            <?php 
                                include("conexion.php");
                                $result = mysqli_query($connect, " select * from mascotas where Id_cliente='".$_SESSION['sessionId']."'");
                                $i = 0;
                                while($row = mysqli_fetch_array($result)){
                                echo '            
                                    <option value="'.$row['Nombre'].'">'.$row['Nombre'].'</option>                  
                                ';
                                $i++;}
                            ?>
                        </select>
                        <input type="submit" class="button button-block" value="Borrar mascota">
                    </form>
                </div>
            </div>
        </div>
    </section>

    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script><script  src="../componentes js//script.js"></script>

</body>
</html>