<?php
session_start();

include("conexion.php");
$con = mysqli_connect($host, $user, $pass, $bd);

// Verificar la conexión
if ($con->connect_error) {
    die("Conexión fallida: " . $con->connect_error);
}

// Verificar si el formulario fue enviado
if (isset($_POST['btnAgregarAlCarro'])) {
    $id_cliente = $_SESSION["sessionId"];
    agregarProductosAlCarro($con, $id_cliente, $_POST);
} else {
    echo "Acceso no autorizado.";
}

// Cerrar la conexión
$con->close();

function agregarProductosAlCarro($con, $id_cliente, $postData) {
    // Obtener los datos del formulario
    $productoIds = $postData['productoId'];
    $cantidades = $postData['productoCantidad'];
    $imagenes = $postData['productoImagen'];
    $descripciones = $postData['productoDescripcion'];
    $precios = $postData['productoPrecio'];
    $stocks = $postData['productoStock'];
    $productoCaracteristicas_1 = $postData['productoCaracteristicas_1'];
    $productoCaracteristicas_2 = $postData['productoCaracteristicas_2'];
    $productoCaracteristicas_3 = $postData['productoCaracteristicas_3'];
    $productoCaracteristicas_4 = $postData['productoCaracteristicas_4'];
    $productoCaracteristicas_5 = $postData['productoCaracteristicas_5'];

    // Preparar la declaración
    $stmt = $con->prepare("INSERT INTO carro_de_compras 
    (id_cliente, producto_id, producto_cantidad, producto_imagen, 
    producto_descripcion, producto_precio, producto_stock, 
    producto_caracteristicas_1, producto_caracteristicas_2, 
    producto_caracteristicas_3, producto_caracteristicas_4, 
    producto_caracteristicas_5, producto_precio_total) 
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

    // Procesar cada producto
    for ($i = 0; $i < count($productoIds); $i++) {
        $cantidad = (int)$cantidades[$i]; // Obtener la cantidad específica de cada producto

        // Validar cantidad
        if ($cantidad <= 0) {
            continue; // Salta si la cantidad no es válida
        }

        $id_producto = $con->real_escape_string($productoIds[$i]);
        $imagen = $con->real_escape_string($imagenes[$i]);
        $descripcion = $con->real_escape_string($descripciones[$i]);
        $precio = (float)$precios[$i];
        $stock = (int)$stocks[$i];
        $precio_total = $precio * $cantidad;

        // Verificar si el producto ya está en el carro
        $repeat = mysqli_query($con, "SELECT * FROM carro_de_compras WHERE producto_id = '$id_producto' AND id_cliente = '$id_cliente'");
        $check = mysqli_fetch_array($repeat);

        if ($check) {
            // Si el producto ya existe, actualizar la cantidad
            $update = "UPDATE carro_de_compras SET 
                        producto_cantidad = producto_cantidad + $cantidad, 
                        producto_precio_total = producto_precio * (producto_cantidad + $cantidad) 
                        WHERE producto_id = '$id_producto' AND id_cliente = '$id_cliente'";
            mysqli_query($con, $update);
        } else {
            // Sanitizar características
            $caracteristicas = [
                !empty($productoCaracteristicas_1[$i]) ? $con->real_escape_string($productoCaracteristicas_1[$i]) : '',
                !empty($productoCaracteristicas_2[$i]) ? $con->real_escape_string($productoCaracteristicas_2[$i]) : '',
                !empty($productoCaracteristicas_3[$i]) ? $con->real_escape_string($productoCaracteristicas_3[$i]) : '',
                !empty($productoCaracteristicas_4[$i]) ? $con->real_escape_string($productoCaracteristicas_4[$i]) : '',
                !empty($productoCaracteristicas_5[$i]) ? $con->real_escape_string($productoCaracteristicas_5[$i]) : ''
            ];

            // Ejecutar la inserción
            $stmt->bind_param("siissdisssssd", $id_cliente, $id_producto, $cantidad, $imagen, 
                              $descripcion, $precio, $stock, 
                              $caracteristicas[0], $caracteristicas[1], 
                              $caracteristicas[2], $caracteristicas[3], 
                              $caracteristicas[4], $precio_total);
            
            if (!$stmt->execute()) {
                echo "Error al agregar el producto $id_producto: " . $stmt->error . "<br>";
            }
        }
    }

    // Cerrar la declaración
    if ($stmt) {
        $stmt->close();
    }

    // Redirigir a la página del carrito
    header("Location: carro.php");
    exit();
}
